#include "hashtable.h"


//Funcion para crear una Tabla Hash 
hashtable *crearHashTable(int numeroBuckets){
	int i;
	hashtable *tabla=NULL;
	if (numeroBuckets < 1) return NULL;
	
	if(( tabla = malloc(sizeof(hashtable)))== NULL){
		return NULL;	
	}
	
	if ((tabla->buckets =malloc( sizeof(objeto *) * numeroBuckets))==NULL)	
	{
		return NULL;
	}
	
	for ( i=0; i<numeroBuckets; i++){
		tabla->buckets[i]=NULL;
	}
	
	tabla->numeroBuckets = numeroBuckets;

	tabla->elementos = 0;


	return tabla;
}
